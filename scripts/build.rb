# frozen_string_literal: true

require 'json'
require 'net/http'

# rubocop:disable Metrics/ClassLength, Style/Documentation

class ImageBuilder
  VERSION_REGEX = /^v?(?<major>\d+)\.?(?<minor>\d+)?\.?(?<patch>\d+)?/.freeze
  IGNORED_IN_PATH = %w[playwright].freeze
  IGNORED_IN_ARGS = %w[glab].freeze

  DOCKERFILES = {
    smocker: <<~DOCKERFILE
      ARG SMOCKER_VERSION=0.18.5
      ARG GOGS_VERSION=0.13.2
      FROM thiht/smocker:${SMOCKER_VERSION} AS smocker
      FROM gogs/gogs:${GOGS_VERSION}
      ENV SMOCKER_TLS_ENABLE=true \
          SMOCKER_TLS_CERT_FILE=/opt/certs/cert.pem \
          SMOCKER_TLS_PRIVATE_KEY_FILE=/opt/certs/key.pem
      COPY --from=smocker /opt /opt
      COPY scripts/gogs/entrypoint /usr/local/bin/entrypoint
      COPY scripts/gogs/app.ini /data/gogs/conf/app.ini
      COPY scripts/gogs/data/gogs.db /app/gogs/data/gogs.db
      COPY scripts/gogs/certs /opt/certs
      RUN cp /opt/certs/cert.pem /usr/local/share/ca-certificates/smocker.crt
      RUN update-ca-certificates
      WORKDIR /opt
      EXPOSE 3443 8080 8081
      HEALTHCHECK CMD (curl -k -o /dev/null -sS https://localhost:3443/healthcheck && curl -o /dev/null -sS http://localhost:${SMOCKER_CONFIG_LISTEN_PORT:-8081}/version) || exit 1
      ENTRYPOINT [ "entrypoint" ]
    DOCKERFILE
  }.freeze

  private_class_method :new

  def initialize
    @dockerfile_path = 'Dockerfile.tmp'
    @registry = ENV['CI_REGISTRY_IMAGE'] || 'registry.gitlab.com/dependabot-gitlab/ci-images'
  end

  def self.run(command)
    new.tap do |builder|
      builder.check_versions
      builder.send(command)
    end
  end

  # Check versions present for all tools
  #
  # @return [void]
  def check_versions
    missing_versions = tool_data.select { |_, version| version.nil? }.keys

    raise(StandardError, "Missing versions for tools: #{missing_versions.join(', ')}") unless missing_versions.empty?
  end

  # Print details of build
  #
  # @return [void]
  def print_details
    log 'Build image'
    puts "#{image}:#{tag}"

    log 'Command'
    puts command.join(' ')

    log 'Dockerfile'
    puts final_dockerfile
  end

  # Build docker image
  #
  # @return [void]
  def build
    create_temp_dockerfile

    exec(command.join(' '))
  end

  # Create multiarch image
  #
  # @return [void]
  def create_multiarch_image
    full_image = "#{image}:#{tag}"
    img_tools_command = ['docker', 'buildx', 'imagetools', 'create', '--tag', full_image]
    img_tools_command.push('--tag', "#{image}:latest") unless tag == 'latest'
    img_tools_command.push("#{full_image}-amd64")
    if system("docker manifest inspect #{full_image}-arm64 > /dev/null 2>&1")
      img_tools_command.push("#{full_image}-arm64")
    end
    cmd = img_tools_command.join(' ')

    log "Creating multiarch image, command: '#{cmd}'"
    exec(cmd)
  end

  private

  # @return [String] path to temporary dockerfile
  attr_reader :dockerfile_path
  # @return [String] image registry
  attr_reader :registry

  # Tool versions
  #
  # @return [Hash]
  def all_tools
    @all_tools ||= JSON
                   .parse(File.read('tool-versions.json'))
                   .transform_values { |data| data['version'] }
  end

  # Data for tools to be built
  #
  # @return [Hash]
  def tool_data
    @tool_data ||= (ENV['TOOLS'] || '')
                   .split(',')
                   .select { |tool| all_tools.keys.include?(tool) }
                   .to_h { |tool| [tool, all_tools[tool]] }
  end

  # Push image to registry
  #
  # @return [Boolean]
  def push?
    @push ||= ENV['PUSH_IMAGE'].to_s.match?(/true|TRUE|1/)
  end

  # Final dockerfile
  #
  # @return [String]
  def final_dockerfile # rubocop:disable Metrics/AbcSize
    return @final_dockerfile if defined?(@final_dockerfile)
    return @final_dockerfile = DOCKERFILES[:smocker] if tool_data.keys.include?('smocker')

    [
      dockerfile_prepends.select { |tool, _| tool_data.keys.include?(tool.to_s) }.values.join("\n"),
      base_dockerfile,
      ruby_dockerfile_append,
      tool_dockerfile_appends.select { |tool, _| tool_data.keys.include?(tool.to_s) }.values.join("\n")
    ].join("\n").strip
  end

  # Base dockerfile
  #
  # @return [String]
  def base_dockerfile
    @base_dockerfile ||= File.read('Dockerfile')
  end

  # Additional custom dockerfile prepends
  #
  # @return [Hash]
  def dockerfile_prepends
    @dockerfile_prepends ||= {}
  end

  # Additional custom dockerfile append sections
  #
  # @return [Hash]
  def tool_dockerfile_appends
    @tool_dockerfile_appends ||= {
      glab: <<~DOCKERFILE,
        # COPY glab and release-cli executables
        COPY --from=registry.gitlab.com/gitlab-org/cli:#{all_tools['glab']} /usr/bin/glab /usr/local/bin/glab
        COPY --from=registry.gitlab.com/gitlab-org/release-cli:v0.22.0 /usr/local/bin/release-cli /usr/local/bin/release-cli
      DOCKERFILE
      kubectl: <<~DOCKERFILE
        # COPY kubectl binary
        COPY --from=registry.k8s.io/kubectl:#{all_tools['kubectl']} /bin/kubectl /usr/local/bin/kubectl
        RUN kubectl version --client
      DOCKERFILE
    }
  end

  # Ruby dockerfile append section
  #
  # @return [String]
  def ruby_dockerfile_append
    <<~DOCKERFILE
      # Install ruby and update rubygems to versions used in dependabot-core
      COPY --from=docker.io/library/ruby:#{ruby_version}-bookworm /usr/local /usr/local
      RUN gem update --system #{rubygems_version} && bundle --version
    DOCKERFILE
  end

  # Buildkit command base
  #
  # @return [Array]
  def command_base
    return @command_base if defined?(@command_base)

    base = [
      'docker', 'buildx', 'build',
      '--platform', "linux/#{arch}",
      '--file', dockerfile_path,
      '--cache-to', 'type=inline',
      '--cache-from', "type=registry,ref=#{image}:latest-#{arch}",
      '--tag', "#{image}:#{tag}-#{arch}"
    ]
    base.push('--tag', "#{image}:latest-#{arch}") unless tag == 'latest'
    base.push('--push') if push?

    @command_base = base << '.'
  end

  # Docker image
  #
  # @return [String]
  def image
    @image ||= "#{registry}#{path}"
  end

  # Image tag based on tool versions
  #
  # @return [String]
  def tag
    return 'latest' if tool_data.empty?

    tool_data
      .reject { |tool| IGNORED_IN_PATH.include?(tool) }
      .map { |_tool, version| major_minor(version) }
      .join('-')
  end

  # Architecture
  #
  # @return [String]
  def arch
    @arch ||= ENV['ARCH'] || `uname -m`.strip
  end

  # Contents of dependabot-core Dockerfile
  #
  # @return [String]
  def core_dockerfile
    @core_dockerfile ||= Net::HTTP.get(
      URI("https://raw.githubusercontent.com/dependabot/dependabot-core/#{all_tools['dependabot-core']}/Dockerfile.updater-core")
    )
  end

  # Docker command
  #
  # @return [Array]
  def command
    tool_data.each_with_object(command_base.dup) do |(tool, version), command|
      next if IGNORED_IN_ARGS.include?(tool)

      command.push("--build-arg #{tool.upcase}_VERSION=#{version}")
    end
  end

  # Image path based on used tools
  #
  # @return [String]
  def path
    return '' if tool_data.empty?

    "/#{tool_data.reject { |tool| IGNORED_IN_PATH.include?(tool) }.keys.join('-').gsub('_', '-')}"
  end

  # Get major.minor version from full version
  #
  # @param [String] version
  # @return [String]
  def major_minor(version)
    version.match(VERSION_REGEX) do |match_data|
      next match_data[:major] unless match_data[:minor]

      "#{match_data[:major]}.#{match_data[:minor]}"
    end
  end

  # Create temporary dockerfile
  #
  # @return [String]
  def create_temp_dockerfile
    File.write(dockerfile_path, final_dockerfile)
  end

  # Log colorized message
  #
  # @param [String] message
  # @return [void]
  def log(message)
    puts "\033[1;35m#{message}\033[0m"
  end

  # Rubygems version of dependabot-core
  #
  # @return [String]
  def rubygems_version
    core_dockerfile.match(/ARG RUBYGEMS_VERSION=(?<version>\d+\.\d+\.\d+)/)[:version]
  end

  # Ruby version of dependabot-core
  #
  # @return [String]
  def ruby_version
    core_dockerfile.match(%r{--from=docker.io/library/ruby:(?<version>\d+\.\d+\.\d+)})[:version]
  end
end

# rubocop:enable Metrics/ClassLength, Style/Documentation

ImageBuilder.run(ARGV[0])
